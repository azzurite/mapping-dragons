# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [0.2.0]

### Added

- Styling
- Javascript rendering

## [0.1.0]

### Added

- Basic web server


[0.2.0]: https://gitlab.com/azzurite/mapsharing/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/azzurite/mapsharing/compare/0e239121...0.1.0
