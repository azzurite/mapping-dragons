(defproject mapping-dragons "0.2.0"
  :description "Directory for maps and related images for tabletop role-playing games"
  :url "https://gitlab.com/azzurite/mapping-dragons"

  :scm {:name "git"
        :url "https://gitlab.com/azzurite/mapping-dragons"}

  :license {:name "GNU General Public License v3.0"
            :url "http://www.gnu.org/licenses/gpl-3.0.html"}

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.10.339"]

                 [compojure "1.6.1"]
                 [com.bhauman/figwheel-main  "0.1.9"]
                 [hiccup "1.0.5"]
                 [garden "1.3.6"]
                 [reagent "0.8.1"]
                 [ring/ring-defaults "0.3.2"]]

  :plugins [[lein-cljsbuild "1.1.7"]
            [lein-pprint "1.2.0"]
            [lein-ring "0.12.4"]]

  :source-paths ["src/clj" "src/cljc" "src/cljs"]
  :test-paths ["test/clj"]
  :target-path "target/%s"
  :clean-targets ^{:protect false} [:target-path
                                    [:cljsbuild :builds :min :compiler :output-dir]]

  :uberjar-name "mapping-dragons.jar"

  :ring {:handler mapping-dragons.handler/app}

  :cljsbuild {:builds {:min {:source-paths ["src/cljs" "src/cljc"]
                             :jar true
                             :compiler {:output-to "resources/public/js/compiled/mapping-dragons.js"
                                        :output-dir "resources/public/js/compiled"
                                        :optimizations :advanced
                                        :pretty-print false
                                        :closure-defines {goog.DEBUG false}
                                        :source-map "resources/public/js/compiled/mapping-dragons.js.map"
                                        :source-map-timestamp true}}}}

  :profiles {:dev {:dependencies [[cider/piggieback "0.3.9"]]
                   :repl-options {:nrepl-middleware [cider.piggieback/wrap-cljs-repl]}}

             :uberjar {:prep-tasks ["compile" ["cljsbuild" "once" "min"]]
                       :aot :all
                       :omit-source true}}

  :release-tasks [["change" "version" "leiningen.release/bump-version"]])
