; Mapping Dragons, a map directory for tabletop RPGs
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns mapping-dragons.render
  (:require [mapping-dragons.global-style :refer [global-styles]]
            [hiccup.page :as hic]))


(def default-page
  (list
    [:head
     (hic/include-css "https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css")
     [:style global-styles]]
    [:body
     [:noscript [:div.noscript [:h1 "Mapping Dragons requires Javascript"]
                 [:p (str "Due to the features we are using and our limited development time, "
                       "Mapping Dragons requires JavaScript to work.")]
                 [:p "In the future our site may become partially usable without Javascript."]
                 [:p (str "Until then, please enable JavaScript (or use a Browser that supports it), "
                       "we promise it's worth it ;)")]]]
     [:div#page]
     (hic/include-js "/js/compiled/mapping-dragons.js")]))

(def not-found-page
  "Not found")

(defn rendered [page]
  (hic/html5 page))
