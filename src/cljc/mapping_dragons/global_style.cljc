; Mapping Dragons, a map directory for tabletop RPGs
; Copyright (C) 2018 Azzurite
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns mapping-dragons.global-style
  (:require [garden.core :refer [css]]
            [garden.units :as u :refer [px pt em]]))

(def ^:private border-box-reset
  '([:html {:box-sizing "border-box"}]
    [:* :*:before :*:after {:box-sizing "inherit"}]))

(def ^:private content-center
  [:body {:display "flex"
          :min-height "100vh"

          :margin 0

          :justify-content "center"
          :align-items "center"}])

(def ^:private noscript-notice
  [:.noscript {:max-width (em 40)
               :text-align "center"}])

(def global-styles
  (css [border-box-reset
        content-center
        noscript-notice]))
