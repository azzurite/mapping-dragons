#!/usr/bin/env bash
set -e -v

if [[ $# == 0 ]]; then
	echo "Usage: ./build/release.sh <level>\n"
	echo "<level> is one of the levels supplied to the \"lein release\" task (:major, :minor, :patch, :alpha, :beta, or :rc)"
	exit 1
fi

if [[ -n "$(git status --porcelain)" ]]; then
	echo "Git working directory is not clean"
	exit 1
fi

function parse_version () {
	lein pprint :version | tr -d '"'
}

function commit () {
	git add -A && git commit -m "$1"
}

function changelog_unreleased_to_version () {
	sed -i "s/## \[Unreleased]/## [$1]/" CHANGELOG.md
	sed -i "s/\[Unreleased]: \(.*\)\.\.\..*/[$1]: \1...$1/" CHANGELOG.md
}

function changelog_add_unreleased_section () {
	sed -i "s/\(## \[$1]\)/## [Unreleased]\n\n\1/" CHANGELOG.md
	sed -i "s/\(\[$1]: \)\(.*compare\/\)\(.*\.\.\..*\)/[Unreleased]: \2$1...develop\n\1\2\3/" CHANGELOG.md
}

git checkout master
git merge --no-ff origin/develop -m "Merge branch 'develop'"
lein change version leiningen.release/bump-version release
version=$(parse_version)
commit "release: set version to ${version}"
changelog_unreleased_to_version "${version}"
commit "docs(changelog): move unreleased changes to ${version}"
git push

git checkout develop
git merge --ff-only master
lein release "$1"
dev_version=$(parse_version)
commit "release: set development version to ${dev_version}"
changelog_add_unreleased_section "${version}"
commit "docs(changelog): add unreleased section"
git push
