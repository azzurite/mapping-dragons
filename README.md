# Mapping Dragons

[![pipeline status](https://gitlab.com/azzurite/mapping-dragons/badges/develop/pipeline.svg)](https://gitlab.com/azzurite/mapping-dragons/commits/develop)

Mapping Dragons is going to be a place to share and find maps for Dungeons & Dragons (and other tabletop roleplaying games). Dungeon maps, battle maps, world maps, objects, tokens or anything related to maps are all accepted content.

## Rationale

The problem with searching for maps online, until now, was not being able to find what you want: people post maps all over the internet, but they never call it "desert hut in sandstorm" but things like "Valorian The Rogue's hideout" - Google will not help there.

This project aims to rectify that by providing a common directory where everyone can upload their maps, but providing a system that makes sure that maps are easily searchable (details not yet set in stone, but probably through a combination of moderation/public voting).

## Main Principles

(in order of importance)

1. Incredibly good organization: ease of finding is the most important thing
   * Meaningful selection of categories/tags is most important
   * Tagging/categorization has to be heavily curated and **not** solely controlled by the uploader: everything **has** to be accurate!
2. Ease of use: everything has to be intuitive and easy to find
3. Licenses on all posts: original author of the image must agree & choose a license
4. Backlinks to all authors
5. Paid content possible, but focus on free
6. Privacy: All data collection opt-in

## Developing

Check out the project and run `lein ring server`.


## Releasing

Releases are triggered through the Gitlab CI jobs `prepare-release` and `finalize-release`. Make sure that all changes are mentioned in the *Unreleased* section of [CHANGELOG.md](CHANGELOG.md) before running these.

## License

Copyright © 2018 Azzurite

Distributed under the [GNU General Public License (GPL) v3.0](LICENSE).
